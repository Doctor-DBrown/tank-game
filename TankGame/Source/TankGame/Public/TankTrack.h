// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class TANKGAME_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	// set a throttle between -1 and +1
	UFUNCTION(BLueprintCallable, Category = "Input")
	void SetThrottle(float Throttle);

	// Max force per track, in Newtwons
	UPROPERTY(EditDefaultsOnly)
	float TrackMaxDrivingForce = 40000000.f; //Assume 40 tonne tank, and 1g acceleration
};
